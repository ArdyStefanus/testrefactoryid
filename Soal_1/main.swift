//
//  main.swift
//  Soal_1
//
//  Created by Ardy Stefanus Sunarno on 23/01/21.
//

import Foundation

var totalPrice: Int = 0

let fbList : [String: Int] = [
                           "NASI":5000,
                           "TAHU":2000,
                           "DAGING":8000,
                           "AIR":2000,
                           "TEH":4000
                        ]
var invoiceList = [String]()

func foodMenu(){
    var fbChoice : String?
    while true {
       print("Hi, we have 5 Food & Beverage options for you!")
       print("----------------------------------------------")
       for (fbCode, fbName) in fbList {
           print("\(fbCode) .......... \(fbName)")
       }
       print("[Q] Back to Main Menu")
       print("")
       print("Your F&B choice? (Code)", terminator:"")
       print("")
       fbChoice = readLine()
       fbChoice = fbChoice?.uppercased()
       
       if fbChoice == "Q"
       {
           break
       }
        
       else
       {
           if let fbName = fbList[fbChoice!] {
               var quantity : String?
               print("How many \(fbName) you want to buy? ", terminator:"")
               quantity = readLine()
            invoiceList.append("\(quantity!) ........ \(fbName)")
            totalPrice = Int(quantity!)!*fbName
               invoiceCart()
           } else {
               print("No Food & Beverage found")
           }
       }
    }
}

func invoiceCart(){
    print("Your Invoice cart : ")
    if invoiceList.isEmpty {
        print("Empty")
    } else {
        for shopCart in invoiceList {
            print("\(shopCart)")
        }
        print("Total Price: \(totalPrice)")
    }
    print("")
}

func mainMenu()
{
    var menuChoice : String?
    
    while true {
        print("=================================")
        print("   Refactory's Cafe & Resto v1.0   ")
        print("=================================")
        print("Options: ")
        print("[1] Buy Food")
        print("[2] Invoice Chart")
        print("[x] Exit")
        print("")
        print("Your choice? ", terminator:"")
        menuChoice = readLine()
        
        switch menuChoice
        {
            case "1":
                foodMenu()
            case "2":
                invoiceCart()
            default:
                break
        }
    }
}

mainMenu()

